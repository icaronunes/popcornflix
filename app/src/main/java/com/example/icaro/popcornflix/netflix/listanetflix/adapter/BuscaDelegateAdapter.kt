package com.example.icaro.popcornflix.netflix.listanetflix.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.example.icaro.popcornflix.R
import com.example.icaro.popcornflix.ReleaseItem
import com.example.icaro.popcornflix.ViewType
import com.example.icaro.popcornflix.ViewTypeDelegateAdapter
import com.squareup.picasso.Picasso
import icaro.com.br.bancomundial.inflate
import kotlinx.android.synthetic.main.news_item_media.view.*

class BuscaDelegateAdapter(val listener: onViewSelectedListener) : ViewTypeDelegateAdapter {


    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder = BuscaViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
        holder as BuscaViewHolder
        holder.bind(item as ReleaseItem)
    }

    inner class BuscaViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(parent.inflate(R.layout.news_item_media)) {

        fun bind(item: ReleaseItem) = with(itemView) {

            Picasso.with(context).load(item.image).into(img_media)
            id_item.text = item.title

            super.itemView.setOnClickListener ({listener.onItemSelected(id = item?.netflixid?.toInt())})
        }
    }

    interface onViewSelectedListener {
        fun onItemSelected(id: Int?)
    }

}

