package com.example.icaro.popcornflix

class Constantes {

    object TipoMedia {
        val MIDIA = 1
        val LOADING = 2
        val ID = "media"
    }

    object API {
        val KEY = "6UGbVdTCYzmshNhukL7cAJrTv5Fnp19wo0fjsnaAGwtIRDdaQ0"
    }

    object JSON {
        val CHEGANDO: String = "chegando"
        val SAINDO: String = "saindo"
    }

}