package com.example.icaro.popcornflix

interface ViewType {
    fun getViewType(): Int
}