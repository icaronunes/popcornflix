package com.example.icaro.popcornflix.netflix.listanetflix.fragment

import android.content.Intent
import android.os.Bundle
import android.support.annotation.Nullable
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.icaro.popcornflix.API
import com.example.icaro.popcornflix.Constantes
import com.example.icaro.popcornflix.R
import com.example.icaro.popcornflix.netflix.listanetflix.adapter.BuscaAdapter
import com.example.icaro.popcornflix.netflix.listanetflix.adapter.BuscaDelegateAdapter
import com.example.icaro.popcornflix.netflix.listanetflix.adapter.InfiniteScrollListener
import com.example.icaro.popcornflix.netflix.netflix.MediaDetalheActivity
import com.example.icaro.popcornflix.netflix.netflixdetalhes.fragment.BaseFragment
import icaro.com.br.bancomundial.inflate
import kotlinx.android.synthetic.main.netflixnews.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


class NetflixNewsFragment : BaseFragment(), BuscaDelegateAdapter.onViewSelectedListener {

    var pagina = 1

    override fun onItemSelected(id: Int?) {
        val intent = Intent(context, MediaDetalheActivity::class.java)
        intent.putExtra(Constantes.TipoMedia.ID, id)
        startActivity(intent)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.netflixnews)
    }

    override fun onActivityCreated(@Nullable savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        news_recyclerview.apply {
            setHasFixedSize(true)
            val linearLayout = GridLayoutManager(context, 2)
            layoutManager = linearLayout
            // addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            addOnScrollListener(InfiniteScrollListener({ getScroll() }, linearLayout))
            news_recyclerview.adapter = BuscaAdapter(this@NetflixNewsFragment)
        }
        getScroll()
    }


    private fun getScroll() {

        val inscricao = API().getNewsLocal(this)
                //.getNews(pagina = pagina, context = this.activity)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    (news_recyclerview.adapter as BuscaAdapter).addNews(it?.items, it?.count, context.applicationContext)
                    ++this.pagina
                }, { erro ->
                    Log.d(javaClass.simpleName, "Erro " + erro.message)
                })
        subscriptions.add(inscricao)
    }
}

