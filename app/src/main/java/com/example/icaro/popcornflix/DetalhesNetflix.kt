package com.example.icaro.popcornflix

import com.google.gson.annotations.SerializedName
import java.io.Serializable




data class DetalhesNetflix(

        @field:SerializedName("RESULT")
        val result: RESULT? = null
) :Serializable

data class CountryItem(

        @field:SerializedName("country")
        val country: String? = null,

        @field:SerializedName("ccode")
        val ccode: String? = null,

        @field:SerializedName("new")
        val new: String? = null,

        @field:SerializedName("seasons")
        val seasons: String? = null,

        @field:SerializedName("expires")
        val expires: String? = null,

        @field:SerializedName("subs")
        val subs: List<String?>? = null,

        @field:SerializedName("audio")
        val audio: List<String?>? = null,

        @field:SerializedName("seasondet")
        val seasondet: List<String?>? = null,

        @field:SerializedName("cid")
        val cid: String? = null

) :Serializable

data class Imdbinfo(

        @field:SerializedName("country")
        val country: String? = null,

        @field:SerializedName("plot")
        val plot: String? = null,

        @field:SerializedName("awards")
        val awards: String? = null,

        @field:SerializedName("imdbid")
        val imdbid: String? = null,

        @field:SerializedName("rating")
        val rating: String? = null,

        @field:SerializedName("genre")
        val genre: String? = null,

        @field:SerializedName("runtime")
        val runtime: String? = null,

        @field:SerializedName("votes")
        val votes: String? = null,

        @field:SerializedName("language")
        val language: String? = null,

        @field:SerializedName("metascore")
        val metascore: String? = null

) :Serializable

data class Nfinfo(

        @field:SerializedName("runtime")
        val runtime: String? = null,

        @field:SerializedName("synopsis")
        val synopsis: String? = null,

        @field:SerializedName("image1")
        val image1: String? = null,

        @field:SerializedName("title")
        val title: String? = null,

        @field:SerializedName("type")
        val type: String? = null,

        @field:SerializedName("image2")
        val image2: String? = null,

        @field:SerializedName("netflixid")
        val netflixid: String? = null,

        @field:SerializedName("unogsdate")
        val unogsdate: String? = null,

        @field:SerializedName("download")
        val download: String? = null,

        @field:SerializedName("matlevel")
        val matlevel: String? = null,

        @field:SerializedName("matlabel")
        val matlabel: String? = null,

        @field:SerializedName("updated")
        val updated: String? = null,

        @field:SerializedName("released")
        val released: String? = null,

        @field:SerializedName("avgrating")
        val avgrating: String? = null
) :Serializable

data class PeopleItem(
        @field:SerializedName("actor")
        val actor: List<String?>? = null
) :Serializable

data class RESULT(

        @field:SerializedName("country")
        val country: List<CountryItem?>? = null,

        @field:SerializedName("nfinfo")
        val nfinfo: Nfinfo? = null,

        @field:SerializedName("Genreid")
        val genreid: List<Any?>? = null,

        @field:SerializedName("mgname")
        val mgname: List<Any?>? = null,

        @field:SerializedName("imdbinfo")
        val imdbinfo: Imdbinfo? = null,

        @field:SerializedName("people")
        val people: List<PeopleItem?>? = null
) :Serializable