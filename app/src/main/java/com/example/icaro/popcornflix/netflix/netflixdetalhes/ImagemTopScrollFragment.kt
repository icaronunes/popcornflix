package com.example.icaro.popcornflix.netflix.netflixdetalhes

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.icaro.popcornflix.R
import com.squareup.picasso.Picasso


class ImagemTopScrollFragment : Fragment() {

    var endereco: String? = null
    var poster: String?  = null

    companion object {
        fun newInstance(backgroud: String, poster: String): Fragment {
            val topScrollFragment = ImagemTopScrollFragment()
            val bundle = Bundle()
            bundle.putString("backgroud", backgroud)
            bundle.putString("poster", poster)
            topScrollFragment.arguments = bundle
            return topScrollFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        endereco = arguments.getString("backgroud")
        poster = arguments.getString("poster")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.page_scroll_viewpage_top, container, false)
        val img_scroll = view.findViewById(R.id.img_top_scroll) as ImageView

        Picasso.with(context).load(endereco)
                .into(img_scroll)

           val animatorSet = AnimatorSet()
           val alphaStar = ObjectAnimator.ofFloat(img_scroll, "y", -100f, 0f)
                  .setDuration(1000)
          animatorSet.playTogether(alphaStar)
         animatorSet.start()

        return view
    }

}