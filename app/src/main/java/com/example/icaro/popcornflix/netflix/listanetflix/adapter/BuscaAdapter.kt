package com.example.icaro.popcornflix.netflix.listanetflix.adapter

import android.content.Context
import android.support.v4.util.SparseArrayCompat
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.Toast
import com.example.icaro.popcornflix.Constantes
import com.example.icaro.popcornflix.ReleaseItem
import com.example.icaro.popcornflix.ViewType
import com.example.icaro.popcornflix.ViewTypeDelegateAdapter

class BuscaAdapter(listener: BuscaDelegateAdapter.onViewSelectedListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var medias: ArrayList<ViewType>
    private var delegateAdapter = SparseArrayCompat<ViewTypeDelegateAdapter>()

    private val loadingItem = object : ViewType {
        override fun getViewType() = Constantes.TipoMedia.LOADING
    }

    init {
        delegateAdapter.put(Constantes.TipoMedia.LOADING, LoadingDelegateAdapter())
        delegateAdapter.put(Constantes.TipoMedia.MIDIA, BuscaDelegateAdapter(listener))
        medias = ArrayList()
        medias.add(loadingItem)
    }

    override fun getItemCount(): Int {
            return medias.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return delegateAdapter.get(viewType).onCreateViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        return delegateAdapter.get(getItemViewType(position)).onBindViewHolder(holder, medias[position])
    }

    override fun getItemViewType(position: Int): Int {
        return medias[position].getViewType()
    }

    fun addNews(news: List<ReleaseItem?>?, count: String?, context: Context) {

        if (medias.size < count?.toInt()!!) {
            val initPosition = medias?.size!! - 1
            medias?.removeAt(initPosition)
            notifyItemRemoved(initPosition)

            if (news != null) {
                for(item in news ){
                    item?.let { medias.add(it) }
                }
            }

            notifyItemRangeChanged(initPosition, medias?.size!! + 1 /* plus loading item */)
            medias.add(loadingItem)
        } else {
            Toast.makeText(context, "Acabou", Toast.LENGTH_LONG).show()
            medias.add(loadingItem)
        }
    }

}

