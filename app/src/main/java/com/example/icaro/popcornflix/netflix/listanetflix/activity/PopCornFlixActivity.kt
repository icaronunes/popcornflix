package com.example.icaro.popcornflix.netflix.netflix

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import com.example.icaro.popcornflix.Constantes
import com.example.icaro.popcornflix.R
import com.example.icaro.popcornflix.ViewType
import com.example.icaro.popcornflix.netflix.listanetflix.adapter.NetflixAdapter
import kotlinx.android.synthetic.main.popcorn_flix.*


class PopCornFlixActivity : AppCompatActivity() {

    private val loadingItem = object : ViewType {
        override fun getViewType() = Constantes.TipoMedia.LOADING
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.popcorn_flix)
        supportActionBar?.title = "NetFlix"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setupViewPagerTabs()
    }

    private fun setupViewPagerTabs() {

        viewPager_person.offscreenPageLimit = 1
        viewPager_person.adapter = NetflixAdapter(supportFragmentManager)
        viewPager_person.currentItem = 0
        tabLayout.setupWithViewPager(viewPager_person)
        tabLayout.tabMode = TabLayout.TEXT_ALIGNMENT_CENTER
    }

}

