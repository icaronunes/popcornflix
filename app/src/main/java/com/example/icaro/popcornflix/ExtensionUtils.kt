package icaro.com.br.bancomundial

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by icaro on 15/06/17.
 */

fun ViewGroup.inflate(layoyt: Int, attachToRoot: Boolean = false ): View? {
    return LayoutInflater.from(context).inflate(layoyt, this, attachToRoot )
}