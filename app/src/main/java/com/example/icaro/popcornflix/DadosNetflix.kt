package com.example.icaro.popcornflix

import com.google.gson.annotations.SerializedName


data class DadosNetflix(

        @field:SerializedName("COUNT")
        val count: String? = null,

        @field:SerializedName("ITEMS")
        val items: List<ReleaseItem?>? = null
) : ViewType {
    override fun getViewType() = Constantes.TipoMedia.MIDIA
}


data class ReleaseItem(

        @field:SerializedName("image")
        val image: String? = null,

        @field:SerializedName("unogsdate")
        val unogsdate: String? = null,

        @field:SerializedName("download")
        val download: String? = null,

        @field:SerializedName("largeimage")
        val largeimage: String? = null,

        @field:SerializedName("imdbid")
        val imdbid: String? = null,

        @field:SerializedName("rating")
        val rating: String? = null,

        @field:SerializedName("runtime")
        val runtime: String? = null,

        @field:SerializedName("synopsis")
        val synopsis: String? = null,

        @field:SerializedName("title")
        val title: String? = null,

        @field:SerializedName("type")
        val type: String? = null,

        @field:SerializedName("netflixid")
        val netflixid: String? = null,

        @field:SerializedName("released")
        val released: String? = null
) : ViewType {
    override fun getViewType() = Constantes.TipoMedia.MIDIA
}


