package com.example.icaro.popcornflix.netflix.listanetflix.adapter

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.icaro.popcornflix.netflix.listanetflix.fragment.NetflixNewsFragment
import com.example.icaro.popcornflix.netflix.listanetflix.fragment.NetflixSaindoFragment

class NetflixAdapter( supportFragmentManager: FragmentManager?) : FragmentPagerAdapter(supportFragmentManager) {

    override fun getItem(position: Int): Fragment? {

        if (position == 0) {
            val f = NetflixNewsFragment()
            val bundle = Bundle()
            bundle.putInt("viewpage", position)
            f.arguments = bundle
            return f
        }
        if (position == 1) {
            val f = NetflixSaindoFragment()
            val bundle = Bundle()
            bundle.putInt("viewpage", position)
            f.arguments = bundle
            return f
        }
        return null
    }

    override fun getCount(): Int = 2

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            "Chegando"
        } else {
            "Saindo"
        }
    }

}

