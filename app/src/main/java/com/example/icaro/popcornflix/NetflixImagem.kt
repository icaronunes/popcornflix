package com.example.icaro.popcornflix

import com.google.gson.annotations.SerializedName

/**
 * Created by icaro on 04/08/2017.
 */

data class NetflixImagem(
        @SerializedName("RESULTS")
        var results: MutableList<RESULTS>? = null
)

data class RESULTS(
        @SerializedName("image")
        var image: MutableList<Image>? = null
)

data class Image(
        @SerializedName("type")
        var type: String? = null,
        @SerializedName("url")
        var url: String? = null,
        @SerializedName("height")
        var height: String? = null,
        @SerializedName("width")
        var width: String? = null
)
