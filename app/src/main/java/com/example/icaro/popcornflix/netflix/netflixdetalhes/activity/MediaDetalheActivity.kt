package com.example.icaro.popcornflix.netflix.netflix

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.util.Log
import com.example.icaro.popcornflix.*
import com.example.icaro.popcornflix.netflix.netflixdetalhes.ImagemTopScrollFragment
import kotlinx.android.synthetic.main.media_detalhe_activity.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


class MediaDetalheActivity : BaseActivity() {

    internal var netflixImagem: List<RESULTS>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.media_detalhe_activity)
        val id = intent.getIntExtra(Constantes.TipoMedia.ID, 0)
        getDados(id)


    }

    private fun getDados(id: Int) {

        val netflix = API().getMediaLocal(this)
                //.getMediaDetalhe(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    val fragmentMediaNetflixDetalhes = FragmentMediaNetflixDetalhe()
                    val bundle = Bundle()
                    bundle.putSerializable("info", it)
                    fragmentMediaNetflixDetalhes.arguments = bundle
                    supportFragmentManager
                            .beginTransaction()
                            .add(R.id.filme_container, fragmentMediaNetflixDetalhes)
                            .commit()

                }, { erro ->
                    Log.d(javaClass.simpleName, "Erro " + erro.message)
                })

        val netflix_imgs = API().getImageLocal(context = this)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    this.netflixImagem = it.results
                    top_img_viewpager?.offscreenPageLimit = 1
                    top_img_viewpager?.adapter = ImagemTopFragment(supportFragmentManager)
                }, { erro ->
                    Log.d(javaClass.simpleName, "Erro IMAGEM " + erro.message)
                })

        subscriptions.add(netflix)
        subscriptions.add(netflix_imgs)
    }

    inner class ImagemTopFragment(supportFragmentManager: FragmentManager) : FragmentPagerAdapter(supportFragmentManager) {

        override fun getItem(position: Int): Fragment? {
            if (netflixImagem?.get(position)?.image?.get(0)?.url != null){
                return ImagemTopScrollFragment.newInstance(netflixImagem?.get(position)?.image?.get(0)?.url!!,
                    netflixImagem?.filter { it?.image!![0].type == "boxart"}?.listIterator(0)?.next()?.image!![0]?.url!!)
            }
        return null
    }

    override fun getCount(): Int {
        if (netflixImagem != null) {
            return netflixImagem?.size!!
        }
        return 0
    }
}

}

