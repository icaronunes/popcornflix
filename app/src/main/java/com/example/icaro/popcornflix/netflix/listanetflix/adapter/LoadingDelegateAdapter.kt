package com.example.icaro.popcornflix.netflix.listanetflix.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.example.icaro.popcornflix.R
import com.example.icaro.popcornflix.ViewType
import com.example.icaro.popcornflix.ViewTypeDelegateAdapter
import icaro.com.br.bancomundial.inflate

class LoadingDelegateAdapter : ViewTypeDelegateAdapter {


    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder = LoadingViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {

    }

    class LoadingViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
            parent.inflate(R.layout.news_item_loading))
}