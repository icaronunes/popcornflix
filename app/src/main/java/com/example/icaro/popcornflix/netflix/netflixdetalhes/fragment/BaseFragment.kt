package com.example.icaro.popcornflix.netflix.netflixdetalhes.fragment

import android.support.v4.app.Fragment
import rx.subscriptions.CompositeSubscription

/**
 * Created by icaro on 02/08/2017.
 */
open class BaseFragment : Fragment() {

    protected var subscriptions = CompositeSubscription()


    override fun onResume() {
        super.onResume()
        subscriptions = CompositeSubscription()
    }

    override fun onPause() {
        super.onPause()
        subscriptions.clear()
    }
}