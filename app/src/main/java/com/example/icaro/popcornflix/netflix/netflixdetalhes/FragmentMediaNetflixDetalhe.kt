package com.example.icaro.popcornflix.netflix.netflix

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.icaro.popcornflix.DetalhesNetflix
import com.example.icaro.popcornflix.Imdbinfo
import com.example.icaro.popcornflix.Nfinfo
import com.example.icaro.popcornflix.R
import com.example.icaro.popcornflix.netflix.netflixdetalhes.fragment.BaseFragment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.info_media.*
import java.text.DecimalFormat
import java.text.NumberFormat


class FragmentMediaNetflixDetalhe : BaseFragment() {

    var detalheFlix: DetalhesNetflix? = null
    var netflix: Nfinfo = Nfinfo()
    var imdb: Imdbinfo = Imdbinfo()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        detalheFlix = arguments.getSerializable("info") as DetalhesNetflix?
        netflix = detalheFlix?.result?.nfinfo!!
        imdb = detalheFlix?.result?.imdbinfo!!
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?)
            = inflater?.inflate(R.layout.info_media, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setAnimacao()
        titulo_text.text = netflix.title
        time_filme.text = netflix.runtime
        Picasso.with(context).load(netflix.image1).into(img_poster)
        descricao.text = imdb.plot
        categoria_filme.text = imdb.genre
        imdb_site.text = "See IMDB "+imdb.metascore
        val nota: NumberFormat = DecimalFormat("0.00")
        val valor = nota.format(netflix.avgrating?.toFloat())
        netflix_site.text = "See Netflix $valor"

    }


    fun setAnimacao() {
        val animatorSet = AnimatorSet()
        val alphaStar = ObjectAnimator.ofFloat(img_star, "alpha", 0f, 1.0f)
                .setDuration(2000)
        val alphaMedia = ObjectAnimator.ofFloat(voto_media, "alpha", 0f, 1.0f)
                .setDuration(2300)
        val alphaBuget = ObjectAnimator.ofFloat(img_budget, "alpha", 0f, 1.0f)
                .setDuration(2500)
        val alphaReviews = ObjectAnimator.ofFloat(icon_reviews, "alpha", 0f, 1.0f)
                .setDuration(2800)
        val alphaSite = ObjectAnimator.ofFloat(icon_site, "alpha", 0f, 1.0f)
                .setDuration(3000)
        val alphaCollecton = ObjectAnimator.ofFloat(icon_collection, "alpha", 0f, 1.0f)
                .setDuration(3300)
        animatorSet.playTogether(alphaStar, alphaBuget, alphaMedia, alphaReviews, alphaSite, alphaCollecton)
        animatorSet.start()
    }
}