package com.example.icaro.popcornflix

import android.app.Activity
import android.content.Context
import android.support.v4.app.Fragment
import android.util.Log
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.Request
import rx.Observable
import java.io.InputStream
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet


class API {

    fun getNews(pais: String = "BR", pagina: Int = 1, tamanho: Int = 20, context: Activity): Observable<DadosNetflix> {
        var json = verificarShared(context, Constantes.JSON.CHEGANDO)
        if (json != "") {
            return getNewsShared(json)
        } else {
            return Observable.create { subscriber ->
                val client = OkHttpClient()
                val gson = Gson()
                val request = Request.Builder()
                        .header("X-Mashape-Key", Constantes.API.KEY)
                        .header("Accept", "application/json")
                        .url("https://unogs-unogs-v1.p.mashape.com/aaapi.cgi?q=get:new$tamanho:$pais&p=$pagina&t=ns&st=adv")
                        .build()
                val response = client.newCall(request).execute()
                if (response.isSuccessful) {
                    json = response.body()?.string()!!
                    // Log.d("Json", json)
                    val news = gson.fromJson(json, DadosNetflix::class.java)
                    if (news.count?.toInt()!! > 0 && pagina == 1) {
                        //   gravarJson(context, nome = Constantes.JSON.CHEGANDO, json = json!!)
                    }
                    subscriber.onNext(news)
                    subscriber.onCompleted()
                } else {
                    subscriber.onError(Throwable(response.message()))
                }
            }
        }
    }


    fun getSaindo(pais: String = "BR", pagina: Int = 1, context: Activity): Observable<DadosNetflix> {
        val json = verificarShared(context, Constantes.JSON.SAINDO)
        if (json != "") {
            return getNewsShared(json)
        } else {
            return Observable.create { subscriber ->
                val client = OkHttpClient()
                val gson = Gson()
                val request = Request.Builder()
                        .header("X-Mashape-Key", Constantes.API.KEY)
                        .header("Accept", "application/json")
                        .url("https://unogs-unogs-v1.p.mashape.com/aaapi.cgi?q=get:exp:$pais&t=ns&st=adv&p=$pagina")
                        .build()
                val response = client.newCall(request).execute()
                if (response.isSuccessful) {
                    val json = response.body()?.string()
                    // Log.d("Json", json)
                    val news = gson.fromJson(json, DadosNetflix::class.java)
                    if (news.count?.toInt()!! > 0 && pagina == 1) {
                        // gravarJson(context = context, nome = Constantes.JSON.SAINDO, json = json!!)
                    }
                    subscriber.onNext(news)
                    subscriber.onCompleted()
                } else {
                    subscriber.onError(Throwable(response.message()))
                }
            }
        }
    }

    private fun getNewsShared(jsonPreference: String): Observable<DadosNetflix> {
        return Observable.create { subscriber ->
            val gson = Gson()
            val numbers = gson.fromJson(jsonPreference, DadosNetflix::class.java)
            if (!numbers?.items?.isEmpty()!!) {
                subscriber.onNext(numbers)
                subscriber.onCompleted()
            } else {
                subscriber.onError(Throwable("Erro não json gravado"))
            }
        }
    }

    private fun verificarShared(context: Context, name: String): String {
        val shared = context.getSharedPreferences(name, Context.MODE_PRIVATE)
        val jsonPreference: Set<String> = shared.getStringSet(Constantes.JSON.CHEGANDO, HashSet<String>())
        if (jsonPreference.isNotEmpty()) {
            val list = ArrayList<String>(jsonPreference)
            val date = Date()
            if (list[1] == Calendar.getInstance().set(date.year, date.month, date.day).toString()) {
                return list[0]
            }
        }
        return ""
    }


    fun getMediaDetalhe(id: Int): Observable<DetalhesNetflix> {
        return Observable.create { subscriber ->
            val client = OkHttpClient()
            val gson = Gson()
            val request = Request.Builder()
                    .header("X-Mashape-Key", Constantes.API.KEY)
                    .header("Accept", "application/json")
                    .url("https://unogs-unogs-v1.p.mashape.com/aaapi.cgi?t=loadvideo&q=$id")
                    .build()
            val response = client.newCall(request).execute()
            if (response.isSuccessful) {
                val json = response.body()?.string()
                // Log.d("Json", json)
                val detalhesNetflix = gson.fromJson(json, DetalhesNetflix::class.java)

                subscriber.onNext(detalhesNetflix)
                subscriber.onCompleted()
            } else {
                subscriber.onError(Throwable(response.message()))
            }
        }
    }


    fun getImagemNetFlix(id: Int): Observable<NetflixImagem> {
        return Observable.create { subscriber ->
            val client = OkHttpClient()
            val gson = Gson()
            val request = Request.Builder()
                    .header("X-Mashape-Key", Constantes.API.KEY)
                    .header("Accept", "application/json")
                    .url("https://unogs-unogs-v1.p.mashape.com/aaapi.cgi?t=images&q=$id")
                    .build()
            val response = client.newCall(request).execute()
            if (response.isSuccessful) {
                val json = response.body()?.string()
                Log.d("Json", json)
                val imagem = gson.fromJson(json, NetflixImagem::class.java)
                subscriber.onNext(imagem)
                subscriber.onCompleted()
            } else {
                subscriber.onError(Throwable(response.message()))
            }
        }
    }


    fun gravarJson(context: Activity, nome: String, json: String) {
        val sharedPref = context.getSharedPreferences(nome, Context.MODE_PRIVATE)
        val edit = sharedPref.edit()
        val date = Date()
        val data = Calendar.getInstance().set(date.year, date.month, date.day)
        var set = HashSet<String>()
        set.add(json)
        set.add(data.toString())
        edit.putStringSet(nome, set)
        edit.apply()
    }

    fun getImageLocal(context: Context, pagina: Int = 1): Observable<NetflixImagem> {
        return Observable.create { subscriber ->
            //     val client = OkHttpClient()
            val gson = Gson()
            var result: String
            try {
                val res = context.resources
                val in_s: InputStream
                in_s = res.openRawResource(R.raw.imagem_netflix)
                val b = ByteArray(in_s.available())
                in_s.read(b)
                result = String(b)
            } catch (e: Exception) {
                // e.printStackTrace();
                result = "Error: can't show file."
            }
            // Log.d("Json", result
            val imagens = gson.fromJson(result, NetflixImagem::class.java)
            if (imagens != null) {
                subscriber.onNext(imagens)
                subscriber.onCompleted()
            } else {
                subscriber.onError(Throwable("Erro na obtenção dos dados."))
            }
        }
    }

    fun getNewsLocal(context: Fragment, pagina: Int = 1): Observable<DadosNetflix> {
        return Observable.create { subscriber ->
            //     val client = OkHttpClient()
            val position = context.arguments["viewpage"]
            val gson = Gson()
            var result: String
            try {
                val res = context.resources
                val in_s: InputStream
                if (position == 0) {
                    in_s = res.openRawResource(R.raw.new_release_per_country)
                } else {
                    in_s = res.openRawResource(R.raw.expiring_per_country)
                }
                val b = ByteArray(in_s.available())
                in_s.read(b)
                result = String(b)
            } catch (e: Exception) {
                // e.printStackTrace();
                result = "Error: can't show file."
            }
            // Log.d("Json", result)

            val numbers = gson.fromJson(result, DadosNetflix::class.java)

            subscriber.onNext(numbers)
            subscriber.onCompleted()
        }
    }

    fun getMediaLocal(context: Context): Observable<DetalhesNetflix> {
        return Observable.create { subscriber ->
            //     val client = OkHttpClient()
            val gson = Gson()
            var result: String
            try {
                val res = context.resources
                val in_s = res.openRawResource(R.raw.load_title_details)
                val b = ByteArray(in_s.available())
                in_s.read(b)
                result = String(b)
            } catch (e: Exception) {
                result = "Error: can't show file."
            }
            val numbers = gson.fromJson(result, DetalhesNetflix::class.java)
            if (numbers != null) {
                subscriber.onNext(numbers)
                subscriber.onCompleted()
            } else {
                subscriber.onError(Throwable("Erro na obtenção dos dados."))
            }
        }
    }

    fun getDetalhesFoto(context: Context): Observable<String?> {
        return getMediaLocal(context)
                .flatMap { filmResults -> Observable.just(filmResults.result) }
                .flatMap { result: RESULT? -> Observable.just(result?.imdbinfo) }
                .flatMap { imdb ->
                    val id = imdb?.imdbid
                    Observable.zip(
                            getId(id.toString()),
                            getImagemNetFlix(id?.toInt()!!)
                                    .asObservable().first()
                            ,{ t1, t2 ->
                        t1
                    }
                    )
                }
    }

    fun getId(id: String): Observable<String>? = Observable.just("id")

}


}